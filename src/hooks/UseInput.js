import { useState } from "react"

const useInput = () => {
    const [text, setText] = useState('');

    const modifyText = (ev) => setText(ev.target.value);
    return{
        text,
        modifyText
    }

}

export default useInput;