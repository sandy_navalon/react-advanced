import './App.scss';
import useCounter from './hooks/Counter'
import useInput from './hooks/UseInput'

function App() {

  const {value,increase,decrease,reset} = useCounter();

  const first = useInput();
  const second = useInput();

  return (
    <div className="App">

      <div className="counter">

        <div className='counter__value'>
          <h1>{value}</h1>
        </div>

        <div className='counter__btnBox'>
          <button className='counter__btn' onClick={increase}>+</button>
          <button className='counter__btn' onClick={decrease}>-</button>
          <button className='counter__btn' onClick={reset}>reset</button>
        </div>

        <div className='counter__input'>
          <h1>->{first.text}</h1>
          <input type='text' onChange={first.modifyText}></input>
        </div>
        <div className='counter__input'>
          <h1>->{second.text}</h1>
          <input type='text' onChange={second.modifyText}></input>
        </div>

        </div>
    </div>
  );
}

export default App;
